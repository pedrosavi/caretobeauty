<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calculator;

class CalculatorController extends Controller
{
    public function calculate(Request $request)
    {
        $operation = $request->display;
        $ip = $this->getUserIp();
        $result = $this->getResult($request->display);
        $bonus = $this->getBonus($result);

        try {
            Calculator::create([
                'ip' => $ip,
                'operation' => $operation,
                'result' => $result,
                'bonus' => $bonus
            ]);
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'Ocorreu um erro ao tentar salvar. Por favor, tente novamente!'
            ];
        }

        return [
            'operation' => $operation,
            'result' => $result,
            'bonus' => $bonus
        ];
    }

    private function getResult($calc)
    {
        $calc   = explode(' ', $calc);
        $result = $calc[0];

        for ($i=1; $i < count($calc); $i++) {
            switch ($calc[$i]) {
                case '+':
                    $result += $calc[$i+1];
                    break;

                case '-':
                    $result -= $calc[$i+1];
                    break;

                case '*':
                    $result *= $calc[$i+1];
                    break;

                case '/':
                    $result /= $calc[$i+1];
                    break;

                case '%':
                    $result %= $calc[$i+1];
                    break;
            }
        }

        return $result;
    }

    private function getUserIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    private function getBonus($result)
    {
        $rand = rand(1, 2);

        return $result === $rand;
    }

}
