require('./bootstrap');

/**
 * Imports
 */

import CalculatorCtrl from './controllers/calculator';

/**
 * Init
 */

window.CalculatorCtrl = CalculatorCtrl;
