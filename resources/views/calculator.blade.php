<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Care To Beauty</title>

        <!-- CSS -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css" >
    </head>
    <body>
        <div class="container">
            <header class="header text-center mt-5 mb-3">
                <h1 class="header__title">Care to Beauty</h1>
                <h2 class="header__subtitle">Calculator</h2>
            </header>

            <form class="calculator" method="POST" action="/calculator/calculate">
                @csrf

                <div class="calculator__header">
                    <input class="calculator__display" name="display" value="0" readonly>
                </div>
                <div class="calculator__body d-flex">
                    <ul class="calculator__number-list d-flex flex-wrap w-75">
                        <li class="calculator__number-list__item order-3"><button type="button" data-number="9">9</button></li>
                        <li class="calculator__number-list__item order-2"><button type="button" data-number="8">8</button></li>
                        <li class="calculator__number-list__item order-1"><button type="button" data-number="7">7</button></li>
                        <li class="calculator__number-list__item order-6"><button type="button" data-number="6">6</button></li>
                        <li class="calculator__number-list__item order-5"><button type="button" data-number="5">5</button></li>
                        <li class="calculator__number-list__item order-4"><button type="button" data-number="4">4</button></li>
                        <li class="calculator__number-list__item order-9"><button type="button" data-number="3">3</button></li>
                        <li class="calculator__number-list__item order-8"><button type="button" data-number="2">2</button></li>
                        <li class="calculator__number-list__item order-7"><button type="button" data-number="1">1</button></li>
                        <li class="calculator__number-list__item w-50 order-12"><button type="button" data-number=".">.</button></li>
                        <li class="calculator__number-list__item w-50 order-11"><button type="reset" data-number="">AC</button></li>
                        <li class="calculator__number-list__item w-100 order-10"><button type="button" data-number="0">0</button></li>
                    </ul>

                    <ul class="calculator__operator-list w-25">
                        <li class="calculator__operator-list__item"><button type="button" data-operator="/">/</button></li>
                        <li class="calculator__operator-list__item"><button type="button" data-operator="-">-</button></li>
                        <li class="calculator__operator-list__item"><button type="button" data-operator="*">x</button></li>
                        <li class="calculator__operator-list__item"><button type="button" data-operator="+">+</button></li>
                        <li class="calculator__operator-list__item"><button type="button" data-operator="%">MOD</button></li>
                        <li class="calculator__operator-list__item"><button type="submit" data-operator="=">=</button></li>
                    </ul>
                </div>
            </form>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>

        <script>
            $(document).ready(function() {
                new CalculatorCtrl().index();
            });
        </script>
    </body>
</html>
