## How to start the project

### If you know Laravel 

- Clone de project
- Go to the folder project
- Run `composer install`
- Run `cp .env.example .env`
- Run `php artisan key:generate`
- Go to .env file and change the database variables
- Run `npm install`
- Run `php artisan migrate`
- Run `php artisan serve`

### If you DON'T know Laravel 

- Unzip the project sent by email
- Import the sql file to your database
- Go to the public folder in de project `cd /path/to/folder/caretobeauty/public`
- Run `php -S localhost:8000`
