export default function () {
    let concatValue = '';

    const setConcatValue = (value) => {
        const operatorsRegex = /( - | \+ | \/ | % | \* )$/;

        if (operatorsRegex.test(value) && operatorsRegex.test(concatValue)) {
            concatValue = concatValue.replace(operatorsRegex, value)
        }
        else if (value === '') {
            concatValue = '';
        }
        else if (value === 0 && (/( \/ | % )$/).test(concatValue)) {
            alert('Cuidado, você não pode fazer divisão por zero!')
        }
        else if (
            (value === '.' && (/\.$/).test(concatValue))
            || (value === '.' && (/(\d+\.\d+|\.\d+)$/).test(concatValue))
            || (operatorsRegex.test(value) && concatValue === '')
            || (value === ' = ')
        ) {
            concatValue = concatValue;
        }
        else {
            concatValue += value.toString();
        }
    };

    const printConcatValue = () => {
        $('.calculator__display').val(concatValue || 0);
    };

    const onClickNumberButton = () => {
        $('.calculator__number-list__item button').on('click', function () {
            const value = $(this).data('number');

            setConcatValue(value);
            printConcatValue();
        });
    };

    const onClickOperatorButton = () => {
        $('.calculator__operator-list__item button').on('click', function () {
            const value = $(this).data('operator');

            setConcatValue(` ${value} `);
            printConcatValue();
        });
    };

    const handleSubmit = () => {
        $('.calculator').on('submit', function (e) {
            const $form = $(this);
            const url = $form.attr('action');
            const method = $form.attr('method');
            const data = $form.serialize();
            const dataType = 'JSON';
            const headers = { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') };

            $.ajax({
                url,
                method,
                headers,
                dataType,
                data
            })
            .fail(function () {
                alert('Ocorreu um erro. Por favor, tente novamente!');
            })
            .done(function (data) {
                concatValue = data.result;
                bonusAlert(data.bonus);
                printConcatValue();
            });

            return false;
        });
    };

    const bonusAlert = (bonus) => {
        if (bonus) {
            alert('Parabens! Você acertou o número bônus.');
        }

    };

    return {
        index: () => {
            onClickNumberButton();
            onClickOperatorButton();
            handleSubmit();
        }
    }
};
